import {
  calculateListFromDAP
} from './'
describe('DAP to List', () => {

  test('49982 -> 36678.91', () => {
    const rego = 391.2;
    const dap = 41950;
    const percentage = 0.03;
    const list = calculateListFromDAP(dap, rego, percentage);
    expect(list.round(2).valueOf()).toBe('36678.91')
  })

  test('9967 -> 8330', () => {
    const rego = 482
    const dap = 9967
    const percentage = 0.035
    const list = calculateListFromDAP(dap, rego, percentage);
    expect(list.round(2).valueOf()).toBe('8330')
  })
})
