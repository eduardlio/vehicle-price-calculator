const b = require('big.js')

export const removeGST = (num) => num.div(11).times(10);

export const addGST = (num) => num.times(1.1);

export const addStamp = (num, percentage) => {
  const stamp = num.times(percentage)
  return num.plus(stamp)
}
const calculateStampOn = (num, percentage) => {
  return num.times(percentage);
}

export const calculateDAP = (lp, regoCharges, percentage) => {
  const listPrice = b(lp)
  const rego = b(regoCharges)

  const egc = addGST(listPrice)
  const stamped = addStamp(egc, percentage)
  const dap = stamped.plus(rego)

  return dap.round(-2, b.roundHalfUp);
}

export const removeStamp = (num, percentage) => {
  return num.div(1 + percentage)
}

export const calculateListFromDAP = (dap, regoCharges, percentage) => {

  const b_dap = b(dap)
  const rego = b(regoCharges)

  const noRego = b_dap.minus(rego)

  const guess = removeStamp(noRego, percentage);
  const rounded = guess.round(-2, b.roundUp)
  const stampOnRounded = calculateStampOn(rounded, percentage);

  const egc = noRego.minus(stampOnRounded);

  return removeGST(egc)

}